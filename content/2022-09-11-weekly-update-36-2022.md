+++
title = "Weekly GNU-like Mobile Linux Update (36/2022): An Update on GNOME Shell on mobile and more"
date = "2022-09-11T21:30:00Z"
draft = false
[taxonomies]
tags = ["PinePhone", "PinePhone Pro", "GNOME Shell", "GNOME Maps", "Sxmo", ]
categories = ["weekly update"]
authors = ["peter"]
[extra]
author_extra = "with help by hamblinggreen, Niko (and friendly assistance from plata's awesome script)"
+++

Camera stuff, why mainlining matters, booting on Pro1X, convergence, GNOME Maps going GTK4, GTK 4.8.0 ... :-)

<!-- more -->
_Commentary in italics._
### Hardware enablement
* [Not happy with the PinePhone's front camera? There may be hope: gc2145 camera driver (front camera on PinePhone) — Linux Kernel](https://www.spinics.net/lists/kernel/msg4507062.html)
* [The Pro1X can now boot postmarketOS](https://twitter.com/RealDanct12/status/1568997719301304326)!

### Software progress

#### GNOME ecosystem
- This Week in GNOME: [#60 Demo Day](https://thisweek.gnome.org/posts/2022/09/twig-60/).
- GNOME Shell & Mutter: [GNOME Shell on mobile: An update](https://blogs.gnome.org/shell-dev/2022/09/09/gnome-shell-on-mobile-an-update/). _Impressive progress!  Regarding GNOME Shell on mobile vs. Phosh, [read my take on Reddit](https://teddit.net/r/Purism/comments/xaa4gt/why_isnt_purism_using_gnome_shell_instead_of_phosh/inszzw7/#c)._
- Sébastien Wilmet: [Crediting people](https://informatique-libre.be/swilmet/blog/2022-09-07-crediting-people.html)
- Marcus Lundblad: [Maps goes four](http://ml4711.blogspot.com/2022/09/maps-goes-four.html). _Curious how this will do on mobile!_


#### Plasma/Maui ecosystem
- Nate Graham: [This week in KDE: Getting Plasma 5.26 ready](https://pointieststick.com/2022/09/09/this-week-in-kde-getting-plasma-5-26-ready/)
- KDE Announcements: [KDE Gear 22.08.1](https://kde.org/announcements/gear/22.08.1/)
- KDE Announcements: [KDE Plasma 5.25.5, Bugfix Release for September](https://kde.org/announcements/plasma/5/5.25.5/)
- Phoronix: [Qt 5.15.6 LTS Open-Source Version Released](https://www.phoronix.com/news/Qt-5.15.6-LTS-Open-Source)
- mardy: [MiTubo 1.3: sorting of QML ListView via Drag&Drop](http://mardy.it/blog/2022/09/mitubo-13-sorting-of-qml-listview-via-dragdrop.html)

#### Ubuntu Touch
- UBports News: [Ubuntu Touch Q&A 120](http://ubports.com/blog/ubports-news-1/post/ubuntu-touch-q-a-120-3863)

#### Sailfish OS
- flypig: [Sailfish Community News, New Harbour APIs](https://forum.sailfishos.org/t/sailfish-community-news-new-harbour-apis/12925)

#### Distributions
* [Glodroid v0.8.1 Released](https://github.com/GloDroid/glodroid_manifest/releases/tag/v0.8.1). _This release does not contain pre-built images yet, so trying it will require more effort than usual._
* [postmarketOS starts to work on packaging Gnome Mobile](https://gitlab.com/postmarketOS/pmaports/-/merge_requests/3404)
* [... and may soon to release it](https://twitter.com/calebccff/status/1568570783823237120)

#### Non-Linux
- [NuttX RTOS for PinePhone: UART Driver](https://lupyuen.github.io/articles/serial). _It's working now!_

#### Matrix
- Matrix.org: [This Week in Matrix 2022-09-09](https://matrix.org/blog/2022/09/09/this-week-in-matrix-2022-09-09)
- [Nheko 0.10.1 Released,](https://github.com/Nheko-Reborn/nheko/releases/tag/v0.10.1) *The addition of matrix community editing support is exciting!*

### Worth noting
- Kyle Rankin on Purism forums: [UPERFECT X Mini lapdock works with Librem 5](https://forums.puri.sm/t/uperfect-x-mini-lapdock-works-with-librem-5/18209). _Convergence, so nice! The matrix chat had an interesting detail regarding attaching the phone to the side of the screen with a magnetic smartphone mount, that might rekindle my own Lap Dock use._ 
- [Gpodder Adaptive 3.11.0+1 released on Flathub](https://flathub.org/apps/details/org.gpodder.gpodder-adaptive). _If your distribution does it ship it, this will enable you to get it installed easily!_
- Phalio: [Sxmo](https://phal.io/tech/sxmo#lockscreen). _Some tips and tricks to improve/customize your Sxmo experience._
- Pinephone Pro cameras [now work with stock Megapixels](https://www.reddit.com/r/pinephone/comments/vpx1vr/comment/in9psyc/?context=3)!


### Worth reading

#### Mainlining things.
* Anjandev Momi: [The Pinephone Pro, PostmarketOS, and Why Mainlining Phones is Important](https://momi.ca/posts/2022-09-07-mainline.html) *Interesting arguments about the long-term support of linux phones, definitely worth a read.*

#### Convergent Marketing
- Purism: [Shape Shifting Computer, Powered by Convergence](https://puri.sm/posts/shape-shifting-computer-powered-by-convergence/)

#### FSFE
* FSFE: [Librem 5: a PC in your pocket +++ Booths are back - FSFE](https://fsfe.org/news/nl/nl-202209.en.html)

#### Sunday morning 

* LINMOB.net: [Sunday Morning Experiment: Video recording on the PinePhone](https://linmob.net/playing-with-pinephone-video-recording/). _If you're interested in this, learn Czech and follow [OpenAlt](https://www.openalt.cz/2022/program_detail.php#event_1) [next weekend](https://twitter.com/xmlich02/status/1568982751042310144). ;-)_

### Worth listening
* Linux, Daily: [JingOS might not be dead yet? by Linux, Daily.](https://anchor.fm/niccolove/episodes/JingOS-might-not-be-dead-yet-e1nei9f). _Nice new podcast by [Niccolò Venerandi](https://twitter.com/veggero)!_

### Worth watching

#### Sxmo micro laptop
* Phalio: [Sxmo Password Lock Screen with PinePhone Keyboard (Sway)](https://tube.tchncs.de/w/ebe15a27-6abf-4f1c-b7da-8a3be14e5099).

#### Pixelfed
* fabrixxm: [Gtk4 wip pixelfed client on pinephone](https://peertube.uno/w/5577f634-aadd-4692-8d47-6bb13d3c38f2)

#### Shape Shifting
* Purism: [Shape Shifting Computer, Powered by Convergence](https://www.youtube.com/watch?v=24NGBhc9nII)

#### Games
* Samuel Venable: [GameMaker Studio 2 Linux Game Running on Manjaro Plasma Mobile PinePhone](https://www.youtube.com/shorts/K3lpq3yV4TQ)

#### postmarketOS
* Jhony Kernel: [postmarketOs nokia n900 2022](https://www.youtube.com/watch?v=VaaVw7t_5PE)
* LaryWulf: [#shorts postmarketos on old phone from 2015 😎hacks](https://www.youtube.com/shorts/8Zb6gN7jyrU)

#### Ubuntu Touch
* Anino ni Kugi: [Sapot Browser | More Features, More Fun, More Bugs???](https://www.youtube.com/shorts/Ro8UiXxIHro). _Looks amazing!_

#### Sailfish OS
* Continuum Gaming: [Microsoft Continuum Gaming E329: A better gallery for Sailfish OS - YouTube](https://www.youtube.com/watch?v=vnW9lyUY53c)

### Thanks

Huge thanks to [hamblingreen](https://hamblingreen.gitlab.io), *Link Collecting Padawan*, and Niko and possible further anonymous contributors for helping with this weeks update - and to Plata for [a nifty set of Python scripts](https://framagit.org/linmob/linmob.frama.io/-/merge_requests/5) that speeds up collecting links from feeds by a lot. 

### Something missing? Want to contribute?
If your project's cool story (or your awesome video or nifty blog post or ...) is missing and you don't want that to happen again, please get in touch via social media or email - or just put it into [the hedgedoc pad](https://pad.hacc.space/7yCLy5a9QyOLWusIFiTt9A) for the next one!

PS: In case you are wondering about the title [...](https://fosstodon.org/@linmob/108516506484897358)

