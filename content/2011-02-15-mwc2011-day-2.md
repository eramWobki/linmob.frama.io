+++
title = "MWC2011: Day 2"
aliases = ["2011/02/15/mwc2011-day-2.html"]
author = "peter"
date = "2011-02-15T22:45:00Z"
layout = "post"
[taxonomies]
tags = ["announcement", "copyleft hardware", "FSO", "HTC Flyer", "MeeGo tablet UX", "pixelqi", "platforms", "SHR", "tablets", "ZTE Light 2"]
categories = ["events"]
authors = ["peter"]
+++

### (Noteworthy) Devices:

* ZTE Light 2: Tablet, 7" __PixelQi__ WSVGA screen, Qualcomm Snapdragon 1GHz single core QSD825*, 512MB Ram, 4GB integrated memory, µSDHC, running Android 2.2 (Froyo), integrated 3G, 2 cameras: <a href="http://pixelqi.com/blog1/2011/02/14/zte-introduces-7%E2%80%9D-tablet-pc-with-pixel-qi%E2%80%99s-sunlight-readable-low-power-display/">PixelQi</a> (ZTE sounds like higher quantities than NotionInk ships (at least compared to what they shipped up to now))
* HTC Flyer: Tablet, 7" WSVGA screen with <b>stylus support</b> (piezoelectric touchscreen?), Qualcomm Snapdragon 1.5GHz QSD8255, 1GB Ram,  32GB internal memory, µSDHC, Android 2.4 (Gingerbread) + Sense, WiFi or 3G + WiFi (no voicecalls, though), 2 cameras: <a href="http://www.netbooknews.com/20015/htc-flyer-now-official-stylus-7-inch-1-5ghz-of-android-goodness/">netbooknews.com</a>
* Fun fact: Samsung won´t release another 7" tablet device, they believe in the 10.1" form factor. Apparently Galaxy Tab did sell below Samsung's expectations.</li></ul>Besides these it was a rather boring day, not many exciting new devices (ok, HTC released several new smartphones, but all Sense, all single core &#8212;&gt; not exiting). 

Besides that&mdash;and rather related to Nokia´s recent announcement to settle with Microsoft Windows Phone 7 than to this Mobile World Congress - open source groups have been attacking Nokia´s move that leaves MeeGo as a playground for the future, check it out at <a href="http://www.techeye.net/mobile/open-source-groups-attack-nokias-divorce-of-meego">techeye.net</a> or read the statement in the <a href="http://blog.shr-project.org/2011/02/free-platforms-go-on-despite-former-giants-nogo.html">SHR blog</a>. Nontheless I do hope that there will be some efforts to port MeeGo to devices like the <a href="http://projects.goldelico.com/p/gta04-main/">GTA04</a> refreshed Openmoko Neo FreeRunner. I will certainly try to start to bring this to a cheap EOL device (still above mid level hardware - it's not the Palm Pre) - announcement will be as soon I have got something to show off. Don´t freak out, though.
